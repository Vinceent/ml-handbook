if (window.innerWidth <= 900) {
    const images = document.querySelectorAll(".container.content img");
    let isClicked = false;
    let scrollPosition = 0;
    let imgWidth = "";
    let imgMargin = "";
    const body = document.querySelector("body");
    curImage = null;
    
    body.addEventListener("click", event => {
        image = event.target;
        if (Array.from(images).includes(image)) {
            curImage = image;
            isClicked = !isClicked;
            if (isClicked) {
                let progress = document.querySelector("#progress");
                let staticProgress = document.createElement("div");
                staticProgress.classList.add("staticProgress");
                body.appendChild(staticProgress);
                staticProgress.style.width = progress.style.width;

                scrollPosition = window.pageYOffset;
                body.style.top = -scrollPosition + "px";
                body.style.position = "fixed";
                body.style.left = "0";
                body.style.right = "0";
                body.style.overflowY = "scroll";

                let parentDiv = document.createElement("div");
                let mainDiv = document.createElement("div");
                p = image.closest("p");
                p.append(mainDiv)
                mainDiv.append(parentDiv)
                parentDiv.append(image)
                imgWidth = image.style.width;
                imgMargin = image.style.margin;
                mainDiv.style.width = (image.offsetWidth + "px");
                mainDiv.style.margin = (imgMargin ? imgMargin : "auto");
                mainDiv.style.height = image.offsetHeight + "px";
                mainDiv.style.backgroundImage = "url('/ml-handbook/shad.png')";
                mainDiv.style.backgroundRepeat = "no-repeat";
                mainDiv.style.border = "1px solid rgba(255,0,0,.5)";
                mainDiv.style.borderRadius = "20px";
                mainDiv.style.backgroundPosition = "center";
                mainDiv.style.backgroundSize = "70%";

                const parentDivLeft = parentDiv.offsetLeft;
                const parentDivTop = parentDiv.offsetTop;
                const parentDivWidth = parentDiv.offsetWidth;

                const imageWidth = image.offsetWidth;
                
                function animate(duration) {

                    let start = performance.now();
                    
                    requestAnimationFrame(function animate() {
                        let timeFraction = (performance.now() - start) / duration;
                        if (timeFraction > 1) timeFraction = 1;

                        timeFraction = Math.pow(timeFraction, 2);
                    
                        parentDiv.style.left = (parentDivLeft - parentDivLeft * timeFraction) + "px";
                        parentDiv.style.top = (parentDivTop +
                            ((scrollPosition + window.innerHeight / 2 - parentDivTop) 
                            * timeFraction ) + "px")
                        parentDiv.style.width = ((parentDivWidth +
                            (body.offsetWidth - parentDivWidth) * timeFraction) + "px");
                        parentDiv.style.transform = "translate(0, " + ((-50*timeFraction) + "%") + ")"

                        image.style.width = (imageWidth + 
                            (body.offsetWidth*1.5 - imageWidth) * timeFraction + "px");

                        console.log(image.style.width)
                        if (timeFraction < 1) {
                        requestAnimationFrame(animate);
                        }
                    
                    });
                }

                
                parentDiv.style.display = "block";
                parentDiv.style.position = "absolute";
                parentDiv.style.zIndex = "1236";
                parentDiv.style.overflow = "scroll";
                parentDiv.style.maxHeight = "70vh";

                image.style.maxWidth = "150%";
                image.style.margin = "0";

                animate(150);

                parentDiv.style.transform = "translate(0, -50%)";
                parentDiv.style.width = "100%";
                parentDiv.style.left = "0";
                parentDiv.style.top = scrollPosition + window.innerHeight / 2 + "px";

                image.style.width = "150%";

                const overlayBlur = document.querySelector(".img-bckg-blur");
                overlayBlur.style.display = "block";
                
                if (parentDiv.offsetHeight < window.innerHeight * 3 / 5) {
                    mainDiv.classList.add("shad-faded");
                }
            }
            else {
                staticProgress = document.querySelector(".staticProgress");
                staticProgress.remove();

                body.style.top = "0";
                body.style.position = "static";
                body.style.overflowY = "auto";

                let parentDiv = image.closest("div");
                parentDiv.replaceWith(...parentDiv.childNodes);
                let mainDiv = image.closest("div");
                mainDiv.replaceWith(...mainDiv.childNodes);
                image.style.maxWidth = "100%";
                if (imgWidth) image.style.width = imgWidth;
                else image.style.width = "none";
                if (imgMargin) image.style.margin = "0 auto 1rem";
                else image.style.margin = "auto";
                const overlayBlur = document.querySelector(".img-bckg-blur");
                overlayBlur.style.display = "none";
                window.scrollTo(0, scrollPosition);
            }
        }
        else {
            if (curImage && isClicked) {
                staticProgress = document.querySelector(".staticProgress");
                staticProgress.remove();

                isClicked = !isClicked;
                image = curImage;

                body.style.top = "0";
                body.style.position = "static";
                body.style.overflowY = "auto";

                let parentDiv = image.closest("div");
                parentDiv.replaceWith(...parentDiv.childNodes);
                let mainDiv = image.closest("div");
                mainDiv.replaceWith(...mainDiv.childNodes);
                image.style.maxWidth = "100%";
                if (imgWidth) image.style.width = imgWidth;
                else image.style.width = "none";
                if (imgMargin) image.style.margin = "0 auto 1rem";
                else image.style.margin = "auto";
                const overlayBlur = document.querySelector(".img-bckg-blur");
                overlayBlur.style.display = "none";
                window.scrollTo(0, scrollPosition);
            }
        }
    })
}